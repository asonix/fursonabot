{
  description = "fursonabot";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
        };
      in
      {
        packages = rec {
          fursonabot = pkgs.callPackage ./fursonabot.nix { };

          default = fursonabot;
        };

        apps = rec {
          dev = flake-utils.lib.mkApp { drv = self.packages.${system}.fursonabot; };
          default = dev;
        };

        devShell = with pkgs; mkShell {
          nativeBuildInputs = [ cargo cargo-outdated clippy gcc rust-analyzer rustc rustfmt ];

          RUST_SRC_PATH = "${pkgs.rust.packages.stable.rustPlatform.rustLibSrc}";
        };
      });
}
