use std::path::{Path, PathBuf};
use url::Url;

#[derive(serde::Deserialize)]
pub(crate) struct Config {
    pub(crate) database: DatabaseConfig,
    pub(crate) mastodon: MastodonConfig,
}

#[derive(serde::Deserialize)]
pub(crate) struct MastodonConfig {
    pub(crate) server: Url,
    pub(crate) client_id: String,
    pub(crate) client_secret: String,
    pub(crate) authorization_code: String,
}

#[derive(serde::Deserialize)]
pub(crate) struct DatabaseConfig {
    pub(crate) path: PathBuf,
}

impl Config {
    pub fn read<P: AsRef<Path>>(path: P) -> Result<Self, crate::error::AnyError> {
        let file = std::fs::read_to_string(path)?;

        let config = toml::from_str(&file)?;

        Ok(config)
    }
}
