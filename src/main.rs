use self::{config::Config, database::Database, session::Session};
use rand::seq::SliceRandom;
use reqwest::Client;
use std::{
    fs::File,
    io::{BufRead, BufReader},
};

mod color;
mod config;
mod database;
mod error;
mod session;

const USER_AGENT: &str = "fursonabot";
const VOWELS: &[char] = &['a', 'e', 'i', 'o', 'u'];

#[tokio::main]
async fn main() -> Result<(), error::AnyError> {
    let config = Config::read("./Config.toml")?;

    let database = Database::open(&config.database)?;

    let client = build_client()?;

    let session = if let Some(token) = database.fetch_token().await? {
        Session::from_token(token, &config.mastodon, client.clone())?
    } else {
        let session = Session::auth(&config.mastodon, client.clone()).await?;

        database.save_token(session.token().to_owned()).await?;

        session
    };

    let fursonas = File::open("./fursona.txt")?;
    let fursonas: Vec<_> = BufReader::new(fursonas).lines().collect::<Result<_, _>>()?;

    loop {
        let last_notification = database.last_notification().await?;

        let mut notifs = session.notifications_since(last_notification).await?;

        while let Some(notif) = notifs.next().await? {
            if notif.content().to_lowercase().contains("give me a fursona") {
                let color = color::generate_color(&client).await?;

                if color.is_empty() {
                    eprintln!("Got empty color!!!");
                    continue;
                }

                let species = fursonas
                    .choose(&mut rand::thread_rng())
                    .expect("fursonas is not empty");

                let colored = if color
                    .chars()
                    .next()
                    .map(|c| VOWELS.contains(&c))
                    .unwrap_or(false)
                {
                    format!("an {}", color)
                } else {
                    format!("a {}", color)
                };

                let text = format!(
                    "@{}, your fursona is {} colored {}",
                    notif.account(),
                    colored,
                    species
                );

                session.reply_to(&text, &notif.status).await?;
            }

            database
                .save_last_notification(notif.id().to_owned())
                .await?;
        }

        tokio::time::sleep(std::time::Duration::from_secs(3)).await;
    }
}

fn build_client() -> Result<Client, crate::error::AnyError> {
    Client::builder()
        .user_agent(USER_AGENT)
        .build()
        .map_err(From::from)
}
