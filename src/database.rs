use crate::{config::DatabaseConfig, error::AnyError};
use sled::{Db, Tree};
use std::sync::Arc;

#[derive(Clone)]
pub(crate) struct Database {
    inner: Arc<Inner>,
}

struct Inner {
    config: Tree,
    state: Tree,
    _db: Db,
}

impl Database {
    pub(crate) fn open(config: &DatabaseConfig) -> Result<Database, AnyError> {
        let db = sled::Config::new().path(&config.path).open()?;

        let config = db.open_tree("fursonabot-config")?;
        let state = db.open_tree("fursonabot-state")?;

        Ok(Database {
            inner: Arc::new(Inner {
                config,
                state,
                _db: db,
            }),
        })
    }

    async fn with_inner<F, T>(&self, f: F) -> Result<T, AnyError>
    where
        F: FnOnce(&Inner) -> Result<T, AnyError> + Send + 'static,
        T: Send + 'static,
    {
        let this = Arc::clone(&self.inner);

        let out = tokio::task::spawn_blocking(move || (f)(&this)).await??;

        Ok(out)
    }

    pub(crate) async fn save_token(&self, token: String) -> Result<(), AnyError> {
        self.with_inner(|inner| inner.save_token(token)).await
    }

    pub(crate) async fn fetch_token(&self) -> Result<Option<String>, AnyError> {
        self.with_inner(Inner::fetch_token).await
    }

    pub(crate) async fn last_notification(&self) -> Result<Option<String>, AnyError> {
        self.with_inner(Inner::last_notification).await
    }

    pub(crate) async fn save_last_notification(&self, id: String) -> Result<(), AnyError> {
        self.with_inner(|inner| inner.save_last_notification(id))
            .await
    }
}

impl Inner {
    fn save_token(&self, token: String) -> Result<(), AnyError> {
        self.config.insert("token", token.as_bytes())?;
        Ok(())
    }

    fn fetch_token(&self) -> Result<Option<String>, AnyError> {
        let Some(token_ivec) = self.config.get("token")? else { return Ok(None); };

        let token = String::from_utf8(token_ivec[..].to_vec())?;

        Ok(Some(token))
    }

    fn last_notification(&self) -> Result<Option<String>, AnyError> {
        let Some(token_ivec) = self.state.get("last_notification")? else { return Ok(None); };

        let token = String::from_utf8(token_ivec[..].to_vec())?;

        Ok(Some(token))
    }

    fn save_last_notification(&self, id: String) -> Result<(), AnyError> {
        self.state.insert("last_notification", id.as_bytes())?;
        Ok(())
    }
}
