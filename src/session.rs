use crate::{
    config::MastodonConfig,
    error::AnyError,
    session::notifications::{NotificationsSince, Page, Status},
};
use reqwest::Client;
use url::Url;

mod notifications;

const TOKEN_PATH: &str = "/oauth/token";
const NOTIFICATIONS_PATH: &str = "/api/v1/notifications";
const STATUSES_PATH: &str = "/api/v1/statuses";

pub(crate) struct Session {
    server: Url,
    token: String,
    client: Client,
}

#[derive(serde::Serialize)]
struct OAuthParams<'a> {
    client_id: &'a str,
    client_secret: &'a str,
    code: &'a str,
    redirect_uri: &'static str,
    grant_type: &'static str,
    scope: &'static str,
}

#[derive(serde::Deserialize)]
struct Token {
    access_token: String,
}

#[derive(Debug)]
struct AuthFailure(String);

#[derive(Debug)]
struct PostFailure(String);

#[derive(serde::Serialize)]
struct StatusForm<'a> {
    status: &'a str,
    visibility: notifications::Visibility,
    in_reply_to_id: &'a str,
}

impl Session {
    fn get(&self, url: &url::Url) -> reqwest::RequestBuilder {
        self.client.get(url.as_str()).bearer_auth(&self.token)
    }

    fn post(&self, url: &url::Url) -> reqwest::RequestBuilder {
        self.client.post(url.as_str()).bearer_auth(&self.token)
    }

    pub(crate) async fn reply_to(&self, text: &str, status: &Status) -> Result<(), AnyError> {
        let mut url = self.server.clone();
        url.set_path(STATUSES_PATH);

        let response = self
            .post(&url)
            .form(&StatusForm {
                status: text,
                visibility: status.visibility,
                in_reply_to_id: &status.id,
            })
            .send()
            .await?;

        if !response.status().is_success() {
            return Err(PostFailure(response.text().await?).into());
        }

        Ok(())
    }

    pub(crate) async fn notifications_since(
        &self,
        since_id: Option<String>,
    ) -> Result<NotificationsSince<'_>, AnyError> {
        let mut url = self.server.clone();
        url.set_path(NOTIFICATIONS_PATH);

        let min_id = if let Some(id) = since_id {
            id
        } else {
            String::new()
        };

        let query = format!("types[]=mention&min_id={}", min_id);
        url.set_query(Some(&query));

        let response = self.get(&url).send().await?;

        let page = Page::try_from_response(response).await?;

        Ok(NotificationsSince {
            session: self,
            current: page,
        })
    }

    pub(crate) fn from_token(
        token: String,
        config: &MastodonConfig,
        client: Client,
    ) -> Result<Self, AnyError> {
        Ok(Self {
            server: config.server.clone(),
            token,
            client,
        })
    }

    pub(crate) async fn auth(config: &MastodonConfig, client: Client) -> Result<Self, AnyError> {
        let mut token_url = config.server.clone();
        token_url.set_path(TOKEN_PATH);

        let response = client
            .post(token_url.as_str())
            .form(&OAuthParams {
                client_id: &config.client_id,
                client_secret: &config.client_secret,
                code: &config.authorization_code,
                redirect_uri: "urn:ietf:wg:oauth:2.0:oob",
                grant_type: "authorization_code",
                scope: "read write",
            })
            .send()
            .await?;

        if !response.status().is_success() {
            return Err(AuthFailure(response.text().await?).into());
        }

        let token: Token = response.json().await?;

        Ok(Self {
            server: config.server.clone(),
            token: token.access_token,
            client,
        })
    }

    pub(crate) fn token(&self) -> &str {
        &self.token
    }
}

impl std::fmt::Display for AuthFailure {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Failed auth: {}", self.0)
    }
}

impl std::fmt::Display for PostFailure {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Failed post: {}", self.0)
    }
}

impl std::error::Error for AuthFailure {}
impl std::error::Error for PostFailure {}
