{ lib
, nixosTests
, rustPlatform
}:

rustPlatform.buildRustPackage {
  pname = "fursonabot";
  version = "0.1.3";
  src = ./.;
  cargoLock.lockFile = ./Cargo.lock;

  nativeBuildInputs = [ ];

  passthru.tests = { inherit (nixosTests) fursonabot; };

  meta = with lib; {
    description = "A simple image hosting service";
    homepage = "https://git.asonix.dog/asonix/fursonabot";
    license = with licenses; [ agpl3Plus ];
  };
}
